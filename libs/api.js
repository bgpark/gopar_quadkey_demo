import axios from "axios";
import { OAUTH_DEFAULT_GRANT_TYPE, OAUTH_SCOPE } from "./constants";

export const apiClient = axios.create({
  baseURL: process.env.NEXT_PUBLIC_API_SERVER_URL,
});

export const getAccessToken = async () => {
  const username = process.env.NEXT_PUBLIC_USERNAME;
  const password = process.env.NEXT_PUBLIC_PASSWORD;
  const clientId = process.env.NEXT_PUBLIC_CLIENT_ID;
  const clientSecret = process.env.NEXT_PUBLIC_CLIENT_SECRET;

  const params = new URLSearchParams();
  params.append("grant_type", OAUTH_DEFAULT_GRANT_TYPE);
  params.append("scope", OAUTH_SCOPE);
  params.append("username", username);
  params.append("password", password);

  const response = await apiClient({
    url: "/api/v1/oauth/token",
    method: "post",
    params: {
      grant_type: OAUTH_DEFAULT_GRANT_TYPE,
      scope: OAUTH_SCOPE,
      username: username,
      password: password,
    },
    auth: {
      username: clientId,
      password: clientSecret,
    },
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  });

  return response.data;
};

export const fetchPlaceAround = async ({
  key,
  token,
  kilometer = 50,
  size = 5,
  cancelToken,
}) => {
  const url = `/api/v1/places/find?lat=0&lat2=0&lon=0&lon2=0&quadkey=${key}&rateMax=25&rateMin=0&orderType=rating&size=${size}&kilometer=${kilometer}`;

  try {
    const response = await apiClient.get(url, {
      cancelToken,
      headers: {
        Authorization: "Bearer " + token,
      },
    });
    return response.data.data;
  } catch (e) {
    return e;
  }
};
