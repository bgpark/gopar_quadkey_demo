export const OAUTH_DEFAULT_GRANT_TYPE = "password";
export const OAUTH_REFRESH_TOKEN_GRANT_TYPE = "refresh_token";
export const OAUTH_SCOPE = "read write";

export const ACCESS_TOKEN = "gagopar_access_token";
export const REFRESH_TOKEN = "gagopar_refreshs_token";
