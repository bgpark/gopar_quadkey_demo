import axios from "axios";
import { useEffect, useState } from "react";
import { getAccessToken, fetchPlaceAround } from "./api";
import { getQuadkeys, log } from "./utils";

function useQuadkey() {
  const [center, setCenter] = useState({ lat: 59.95, lng: 30.33 });
  const [zoom, setZoom] = useState(11);
  const [token, setToken] = useState({});
  const [places, setPlaces] = useState([]);
  const CancelToken = axios.CancelToken;
  let source;

  const onBoundsChange = async ({ center, zoom, bounds, ...other }) => {
    log("center, zoom, bounds, other", center, zoom, bounds, other, event);
    if (zoom < 6) return;
    if (!window.maps || !window.map) return;
    if (source) {
      source.cancel("request has been canceled");
    }
    source = CancelToken.source();
    renderPoiByQuadkey(bounds, zoom);
  };

  const loadGoogleMapApi = async (map, maps) => {
    window.map = map;
    window.maps = maps;
    setToken(await getAccessToken());
  };

  const renderPoiByQuadkey = async (bounds, zoom) => {
    fetchAllPoi(getQuadkeys(bounds, zoom));
    // renderEach(quadkeys);
  };

  const fetchAllPoi = (quadkeys) => {
    const placesPromise = quadkeys.map((quadkey) =>
      fetchPlaceAround({
        key: quadkey,
        token: token.access_token,
        kilometer: 50,
        size: 5,
        cancelToken: source.token,
      })
    );

    Promise.all(placesPromise).then((place) => {
      if (place.some((p) => axios.isCancel(p))) return;
      setPlaces(sortPlace(place));
    });
  };

  const sortPlace = (place) => {
    return place
      .filter(Boolean) // undefined 제거
      .filter(removeEmptyArray)
      .map((p) =>
        p.sort(descByRating).map((t, i) => ({ ...t, thumbnail: i == 0 }))
      )
      .flat();
  };

  const descByRating = (a, b) => b.rating - a.rating;
  const removeEmptyArray = (list) => list.length != 0;

  useEffect(() => {}, [places]);

  return {
    center,
    zoom,
    places,
    onBoundsChange,
    loadGoogleMapApi,
  };

  //   const renderEach = (quadkeys) => {
  //     quadkeys.map((quadkey, i) => {
  //       getPlaceAround({ key: quadkey, token: token.access_token }).then(
  //         (response) => {
  //           setPlaces((oldPlaces) => [...oldPlaces, ...response]);
  //           response.map((poi) => {
  //             // renderMarker(poi);
  //           });
  //         }
  //       );
  //     });
  //   };
}

export default useQuadkey;
