import merc from "mercator-projection";

export const log = (message, ...data) => {
  console.log(`[ LOG ] ${message} : `, ...data);
};

export const getRandom = (number) => Math.floor(Math.random() * number);

/* tile -> quadkey */
export const TileXYToQuadKey = (tileX, tileY, zoom) => {
  var quadKey = [];
  for (var i = zoom; i > 0; i--) {
    var digit = 0;
    var mask = 1 << (i - 1);

    if ((tileX & mask) != 0) {
      digit++;
    }

    if ((tileY & mask) != 0) {
      digit += 2;
    }
    quadKey.push(digit);
  }
  return quadKey.join("");
};

export const getQuadkeys = (bounds, zoom) => {
  /* 타일 크기 */
  const TILE_SIZE = 256;

  /* latlng 좌표 */
  const nw = bounds.nw;
  const se = bounds.se;
  const scale = 1 << zoom;

  /* world 좌표 */
  const worldNW = merc.fromLatLngToPoint(nw);
  const worldSE = merc.fromLatLngToPoint(se);

  /* pixel 좌표 */
  const pixelNW = new window.maps.Point(
    Math.floor(worldNW.x * scale),
    Math.floor(worldNW.y * scale)
  );
  const pixelSE = new window.maps.Point(
    Math.floor(worldSE.x * scale),
    Math.floor(worldSE.y * scale)
  );

  /* tile 좌표 */
  const tileNW = new window.maps.Point(
    Math.floor(pixelNW.x / TILE_SIZE),
    Math.floor(pixelNW.y / TILE_SIZE)
  );
  const tileSE = new window.maps.Point(
    Math.floor(pixelSE.x / TILE_SIZE),
    Math.floor(pixelSE.y / TILE_SIZE)
  );

  /* 지도의 가로, 세로 tile의 개수*/
  const col = tileSE.x - tileNW.x + 1;
  const row = tileSE.y - tileNW.y + 1;

  let tiles = [];
  for (let i = 0; i < row; i++) {
    for (let j = 0; j < col; j++) {
      tiles.push({
        x: tileNW.x + j,
        y: tileNW.y + i,
      });
    }
  }

  /* quadkey */
  const quadkeys = tiles.map((tile) => TileXYToQuadKey(tile.x, tile.y, zoom));

  // log("nw, se, tileNW, tileSE", nw, se, tileNW, tileSE);
  // log("row, col, tiles, quadkeys", row, col, tiles, quadkeys);

  return quadkeys;
};
