import GoogleMapReact from "google-map-react";
import React from "react";
import useQuadkey from "../libs/useQuadkey";
import Marker from "../components/Marker";

const index = () => {
  const {
    center,
    zoom,
    places,
    onBoundsChange,
    loadGoogleMapApi,
  } = useQuadkey();

  return (
    <div style={{ height: "100vh", width: "100%" }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: "AIzaSyCKWaug5OXW9Xp6qLfe1oBTyXEfIgupzBg" }}
        defaultCenter={center}
        defaultZoom={zoom}
        onChange={onBoundsChange}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) => loadGoogleMapApi(map, maps)}
      >
        {places.map((place) => (
          <Marker
            key={place.id}
            lat={place.location.lat}
            lng={place.location.lon}
            place={place}
            thumbnail={place.thumbnail && place.rating && place.rating > 0.005}
          />
        ))}
      </GoogleMapReact>
    </div>
  );
};

export default index;
