import axios from "axios";
import React from "react";

const test = () => {
  const CancelToken = axios.CancelToken;
  const source = CancelToken.source();

  const onClickRequest = async () => {
    Promise.all([fetchDifPlace(50), fetchDifPlace(30), fetchDifPlace(20)]).then(
      (place) => {
        console.log(place);
      }
    );
  };

  const fetchDifPlace = async (killometer) => {
    const response = await axios
      .get(
        `https://dev-api.gagopar.com/api/v1/places/find?lat=0&lat2=0&lon=0&lon2=0&quadkey=1202200110033&rateMax=25&rateMin=0&orderType=rating&size=5&kilometer=${killometer}`,
        {
          cancelToken: source.token,
          headers: {
            Authorization:
              "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYXBpIl0sInVzZXJfbmFtZSI6ImNpdHVzIiwic2NvcGUiOlsicmVhZCIsIndyaXRlIl0sIm5hbWUiOiJjaXR1cyIsImV4cCI6MTYxMTI0MzU0NSwidXNlcklkIjo3LCJhdXRob3JpdGllcyI6WyJBRE1JTiJdLCJqdGkiOiI1YWRiYTA5Ny02ZTI1LTQ0ODQtYTU4My1kZmJkNzRmYzFjY2EiLCJjbGllbnRfaWQiOiJ0ZXN0In0.G1E2A6H6cjz5N1FHGKDCNCFECyIBOKxfE2tIj7DbcnI",
          },
        }
      )
      .catch((e) => {
        if (axios.isCancel(e)) {
          console.log("cancel!", e);
        }
      });
    console.log(response);
    return response;
  };

  const onClickAbort = () => {
    source.cancel("Operation canceled by the user.");
  };

  return (
    <div>
      <button onClick={onClickRequest}>요청</button>
      <button onClick={onClickAbort}>취소</button>
    </div>
  );
};

export default test;
