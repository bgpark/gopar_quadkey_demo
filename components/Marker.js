import React, { useCallback } from "react";
import styled from "styled-components";

const ICON_COLORS = ["#ffc9c9", "#d0bfff", "#d0ebff", "#d8f5a2", "#ffec99"];
const DOT_COLORS = ["#ff6b6b", "#845ef7", "#339af0", "#12b886", "#fab005"];
const SVG_NONE =
  "https://test-alice.s3.ap-northeast-2.amazonaws.com/icon/cat/none.svg";
const SVG_TRAVELING =
  "https://test-alice.s3.ap-northeast-2.amazonaws.com/icon/cat/traveling.svg";

const MarkerWrapper = styled.div`
  position: absolute;
  z-index: ${(param) => {
    if (param.thumbnail) return 100;
    if (param.icon != SVG_NONE && param.icon != SVG_TRAVELING) return 10;
    return 1;
  }};
`;

const Thumbnail = styled.img`
  width: 3rem;
  border-radius: 100%;
  border: 3px solid white;
`;

const Icon = styled.img`
  width: 1rem;
  border-radius: 100%;
  background: ${(param) => ICON_COLORS[param.random]};
  border: 2px solid white;
  padding: 3px;
`;

const Dot = styled.div`
  width: 7.5px;
  height: 7.5px;
  border-radius: 100%;
  background: ${(param) => DOT_COLORS[param.random]};
  border: 2px solid white;
`;

const Marker = ({ thumbnail, place }) => {
  const renderThumbnail = useCallback(
    (place) =>
      place.thumbnail_url ? (
        <Thumbnail src={place.thumbnail_url} />
      ) : (
        renderIcon(place)
      ),
    []
  );

  const renderIcon = useCallback((place) => {
    const random = getRandomColor(place.id);
    return place.categoryIconUrl != SVG_NONE &&
      place.categoryIconUrl != SVG_TRAVELING ? (
      <Icon src={place.categoryIconUrl} random={random} />
    ) : (
      <Dot random={random}></Dot>
    );
  }, []);

  const getRandomColor = (id) => {
    const index = id.substr(id.indexOf(":") + 1);
    return Math.floor(index % 5);
  };

  return (
    <MarkerWrapper thumbnail={thumbnail} icon={place.categoryIconUrl}>
      {thumbnail ? renderThumbnail(place) : renderIcon(place)}
    </MarkerWrapper>
  );
};

export default Marker;
